import React from 'react';
import './Botao.css'

function Botao(props) {

    return(
        <button onClick={e => props.click && props.click(props.rotulo)}>
            {props.rotulo}
        </button>
    )
};

export default Botao