import React from 'react';
import Botao from '../Botao/Botao.jsx'
import './Calculadora.css'

function Calculadora() {
    const adicionarDigito = (digito) => {
        console.log(digito)
    }
    return(
        <div id="calculadora">
            <Botao rotulo="AC" click={adicionarDigito}/>
            <Botao rotulo="/" click={adicionarDigito}/>
            <Botao rotulo="7" click={adicionarDigito}/>
            <Botao rotulo="8" click={adicionarDigito}/>
            <Botao rotulo="9" click={adicionarDigito}/>
            <Botao rotulo="*" click={adicionarDigito}/>
            <Botao rotulo="4" click={adicionarDigito}/>
            <Botao rotulo="5" click={adicionarDigito}/>
            <Botao rotulo="6" click={adicionarDigito}/>
            <Botao rotulo="-" click={adicionarDigito}/>
            <Botao rotulo="1" click={adicionarDigito}/>
            <Botao rotulo="2" click={adicionarDigito}/>
            <Botao rotulo="3" click={adicionarDigito}/>
            <Botao rotulo="+" click={adicionarDigito}/>
            <Botao rotulo="0" click={adicionarDigito}/>
            <Botao rotulo="." click={adicionarDigito}/>
            <Botao rotulo="=" click={adicionarDigito}/>
        </div>
    )
};

export default Calculadora